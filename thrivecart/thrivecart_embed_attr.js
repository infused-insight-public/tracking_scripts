// This code should be included on pages that embed Thrive Cart checkouts
// using the iFrame embed code.
//
// IMPORTANT: 
// This code needs to be AFTER jQuery and thrivecart-embeddable div
// But BEFORE the thrive cart embed script

var itracker_forwarding = {
    "customfield1": window.location.href
};

if(window.iTracker360 === undefined) {
    console.error('ThriveCart Attribution - ERROR: iTracker not on page');
    if (window.location.href.indexOf('attr_debug=1') >= 0) {
        itracker_forwarding['attr_passed'] = 'without_itracker';
    }
} else {
    itracker_forwarding['ga_medium'] = window.iTracker360.i.med;
    itracker_forwarding['ga_source'] = window.iTracker360.i.sou;
    itracker_forwarding['ga_campaign'] = window.iTracker360.i.cam;
    itracker_forwarding['ga_term'] = window.iTracker360.i.ter;
    itracker_forwarding['ga_content'] = window.iTracker360.i.con;
    itracker_forwarding['ga_flpurl'] = window.iTracker360.i.firstlpurl;

    if (window.location.href.indexOf('attr_debug=1') >= 0) {
        itracker_forwarding['attr_passed'] = 'all';
    }
}

iframe_query_str = jQuery.param(itracker_forwarding);

// Add the querystring attribute to all thrivecart embeds
jQuery('.thrivecart-embeddable').attr('data-thrivecart-querystring', iframe_query_str)
