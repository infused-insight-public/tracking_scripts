// This script passes UTM parameters into a hidden field on thrivecart
// It should be embeded in the
// ThriveCart -> Product -> Checkout -> Tracking -> `Checkout page:` field
//
// IMPORTANT: 
// It must be put on the page AFTER the iTracker code

function getUrlParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.href);
    return results === null ? null : decodeURIComponent(results[1].replace(/\+/g, '    '));
};

var itracker_medium = null;
var itracker_source = null;
var itracker_campaign = null;
var itracker_term = null;
var itracker_content = null;
var itracker_landing_url = null;
var itracker_order_url = null;

if(window.iTracker360 !== undefined) {
    itracker_medium = window.iTracker360.i.med;
    itracker_source = window.iTracker360.i.sou;
    itracker_campaign = window.iTracker360.i.cam;
    itracker_term = window.iTracker360.i.ter;
    itracker_content = window.iTracker360.i.con;
    itracker_landing_url = window.iTracker360.i.firstlpurl;
    itracker_order_url = window.iTracker360.i.customfield1 ;
} else {
    console.error('ThriveCart Attribution - ERROR: iTracker not on page. Will try to use query parameters.');
}

// Create attribution JSON
var attr_data = {};

if(getUrlParameter('attr_debug') == 1) {
    attr_data['attr_passed'] = getUrlParameter('attr_passed') || 'none';
    attr_data['itracker_on_tc'] = window.iTracker360 === undefined ? 0 : 1;
}

attr_data['utm_medium'] = (
    itracker_medium ||
    getUrlParameter('ga_medium') ||
    getUrlParameter('utm_medium') ||
    ''
);
attr_data['utm_source'] = (
    itracker_source ||
    getUrlParameter('ga_source') ||
    getUrlParameter('utm_source') ||
    ''
);
attr_data['utm_campaign'] = (
    itracker_campaign ||
    getUrlParameter('ga_campaign') ||
    getUrlParameter('utm_campaign') ||
    ''
);
attr_data['utm_term'] = (
    itracker_term ||
    getUrlParameter('ga_term') ||
    getUrlParameter('utm_term') ||
    ''
);
attr_data['utm_content'] = (
    itracker_content ||
    getUrlParameter('ga_content') ||
    getUrlParameter('utm_content') ||
    ''
);
attr_data['landing_url'] = (
    itracker_landing_url ||
    getUrlParameter('ga_flpurl') ||
    ''
);
attr_data['order_url'] = (
    itracker_order_url ||
    getUrlParameter('customfield1') ||
    window.location.href
);

var attr_json_str = JSON.stringify(attr_data); 

// Add the attribution to the attributiondata hidden field 
$('#field-customer-custom-attributiondata').val(attr_json_str);
