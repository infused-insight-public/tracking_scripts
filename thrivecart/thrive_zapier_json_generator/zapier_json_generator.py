import json
import html


def unescape_str(value):
    # Sometimes nested data comes in as a string with HTML escaped characters
    # like `'{&quot;utm_medium&quot;:&quot;cpc&quot;}`
    # So here we attempt to unescape it
    try:
        value_unescaped = html.unescape(value)
    except:
        value_unescaped = value

    return value_unescaped


def json_decode_str(value):
    # Decode JSON strings into python objects
    # This will ensure they end up as valid json
    # in the final output instead of a string of
    # json
    try:
        value_decoded = json.loads(value)
    except:
        value_decoded = value

    return value_decoded


def decode_value(value):
    value_unescaped = unescape_str(value)
    value_decoded = json_decode_str(value_unescaped)

    return value_decoded


def process_zapier_data(input_data):
    input_decoded = {}

    for key, value in input_data.items():
        input_decoded[key] = decode_value(value)

    # Thrive sometimes sends the data as a nested dict into zapier
    # And sometimes as a HTML encoded string
    # When it's an HTML encoded string, the data is in the 'attribution_data'
    # dict.
    # But when it is a nested dict, then it's in seperate keys.
    # Here we normalize make sure the final json has an attribution_data key
    # with all the information
    #
    # Add the keys in the zap input using a path like
    # {{customer__custom_fields__attributiondata__utm_medium}}
    #
    # This allows you to access keys that are not present in zapier test data

    attr_keys = [
        'utm_medium',
        'utm_source',
        'utm_campaign',
        'utm_term',
        'utm_content',
        'landing_url',
        'order_url',
    ]

    attr_data = input_decoded.get('attribution_data', {})
    for attr_key in attr_keys:
        # Get the separate value (if present) and remove it from
        # the main dict
        attr_value = input_decoded.pop(attr_key, None)
        if attr_value is not None:
            attr_data[attr_key] = attr_value
    input_decoded['attribution_data'] = attr_data

    # Convert python object into nicely formatted json string
    output_json_str = json.dumps(input_decoded, indent=4, sort_keys=True)

    # Provide output to next task
    output = {
        'data': output_json_str,
    }

    return output


output = process_zapier_data(input_data)
