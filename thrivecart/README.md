# Thrive Cart Attribution Scripts

## Description

These scripts allow you to pass UTM parameters and attribution info from a landing page into an embedded Thrive Cart Checkout iFrame.

## How to set it up

### Step 1: Add an attribution_data custom field to the Checkout page

* Go to ThriveCart -> Product -> Edit Checkout
* Click the form element
* In the sidebar tick `Add custom fields?`
* Click the `Set up custom fields` button
* Add the custom field with Field ID as `attribution_data` and Label for your customers as `Additional Information`
* Do not tick `Include in the customer's invoice?`
* Press `Save`
* Drag a `Custom HTML` elememt _directly_ under the form element
* In the sidebar add the following code...

```html
<script>
  // Hide custom fields from user unless query param attr_debug is present
  if (window.location.href.indexOf("attr_debug=1") == -1) {
    var custom_fields = document.getElementsByClassName('custom-fields')[0];
    custom_fields.style.display = 'none';
  }
</script>
```

* That code will hide the custom fields so that customers don't see the attribution data. But if you load the page with the `attr_debug=1` paramater, the field won't be hidden and you can check if attribution is working
* Press `Save & Exit`

### Step 2: Add iTracker and thrivecart_attr.js to the Thrive Cart Product & Funnel

* Go to ThriveCart -> Product -> Checkout -> Tracking
* Add the iTracker code in the `All pages:` field
* Add the `thrivecart_attr.js` script to the `Checkout page` field:

```html
<script type="text/javascript" src="https://yourdomain.com/thrivecart_attr.js"></script>
```

### Step 3: Add the Thrive Cart embed code with tracking info to your landing page

Get the Thrive Cart embed code for your product. It will look something like this:

```html
<div data-thrivecart-account="marisapeer" data-thrivecart-tpl="v2" data-thrivecart-product="493" class="thrivecart-embeddable" data-thrivecart-embeddable="tc-marisapeer-493-0U52IS"></div><script async src="//tinder.thrivecart.com/embed/v1/thrivecart.js" id="tc-marisapeer-493-0U52IS"></script>
```

It contains two parts:

1. The first part starts with `<div data-...`. This is where the iFrame will be placed and where the embed settings are defined
2. The javascript code that creates the iFrame from the settings in the `<div data-...` code

The page needs to add the attribution data to the `<div data-...` code so that it's passed into the checkout iFrame.

To do that you need to embed the `thrivecart_embed_attr.js` code like this:

```html
<script type="text/javascript" src="https://yourdomain.com/thrivecart_embed_attr.js"></script>
```

It will take the tracking data from iTracker360 and add it to the div.

For it to work, it needs to be added AFTER the `<div data-...`, but BEFORE the `<script async...` part of the ThriveCart embed code.

You must also adjust `src="https://yourdomain.com/thrivecart_embed_attr.js"` to the actual path where the script is hosted.

In the end the ful embed code should look like this:

```html
<div data-thrivecart-account="marisapeer" data-thrivecart-tpl="v2" data-thrivecart-product="493" class="thrivecart-embeddable" data-thrivecart-embeddable="tc-marisapeer-493-0U52IS"></div>

<script type="text/javascript" src="https://yourdomain.com/thrivecart_embed_attr.js"></script>

<script async src="//tinder.thrivecart.com/embed/v1/thrivecart.js" id="tc-marisapeer-493-0U52IS"></script>
```

## How to test it

Visit the URL of the page where you embedded the Thrive Cart Checkout, but add the query parameter `attr_debug=1` at the end.

So if your URL is `https://www.yourdomain.com/landing-page/` then visit `https://www.yourdomain.com/landing-page/?attr_debug=1`.

When that parameter is present it will show the attribution_data custom field with the content that will be sent to zapier.

It will also add two additional debug keys:

1. `attr_passed`, which is set to `all` if iTracker information was passed through to ThriveCart, `without_itracker` if itracker was not present on the landing page and `none` if no data was passed into Thrive Cart
2. `itracker_on_tc`, which is set to `1` if the iTracker code is present in the ThriveCart product and `0` if it isn't

You want `attr_passed` to be `all` and `itracker_on_tc` to be `1`.
