# Thrive Cart Attribution Scripts

## Description

These scripts allow you to pass UTM parameters and attribution info from a landing page into an embedded Acuity Scheduling iFrame.

## How to set it up

### Step 1: Create an Intake Form with an attr_data field in Acuity

* Go to Acuity -> [Intake Form Questions](https://secure.acuityscheduling.com/forms.php)
* Press the `New Custom Form` button
* Tick `Internal Use Only -> This form is for internal use only, don't show it to clients`
* Tick `Show this form when scheduling: -> Select All`
* Add a text box with question `attr_data` and set it to size `Large (several paragraphs)`
* Right click on the field and select `Inspect` to see the HTML code of the form
* Look for a line like `<li id="field_8644476" class="form-field margin-top field-hidden ">`
* Copy the id number (In this case `8644476`)
* Add it to the code in `acuity_embed_attr.js` or make sure you don't forget to add it as a setting in every embed using the setting attribute `data-acuity-attr_data_field_id`

### Step 2: Add the customized embed code to the appointment page

Get the default Acuity Scheduler embed code. It will look something like this:

```html
<iframe id="acuityiframe" src="//app.acuityscheduling.com/schedule.php?owner=14217432&amp;appointmentType=4060174" height="800" frameborder="0" style="max-height: none; overflow: hidden; height: 1890px !important; width: 100% !important"></iframe>
```

You need to turn it into a code like this:

```html
<!-- iFramed Embed Settings -->
<div
    class="acuity-container"
    data-acuity-owner="14217432"
    data-acuity-appointment-type="4060174"
    data-funnel-step="XXXX"
    data-attr-id="acuityiframe"
    data-attr-height="800"
    data-attr-frameborder="0"
    data-attr-style="max-height: none; overflow: hidden; height: 1890px !important; width: 100% !important"
></div>

<!-- Script that turns above div into iFrame -->
<script type="text/javascript" src="https://infusedinsight.com/extra/tracking_scripts/acuityscheduling/acuity_embed_attr.js"></script>
```

Here's how the settings are set:

* `class="acuity-container"` (Required):
  * This tells the embed javascript that the iFrame should be added within this div
* `data-acuity-owner="14217432"` (Required):
  * Extract this ID number from the original iFrame src URL's `owner` parameter (in the above example it was `owner=14217432`)
* `data-acuity-appointment-type="4060174"` (Required):
  * Extract this ID number from the original iFrame src URL's `appointmentType` parameter (in the above example it was `appointmentType=4060174`)
* `data-funnel-step="XXX"` (Optional):
  * You can define this value yourself
  * It will be used to associate the appointment bookings with a specific step of a specific funnel
  * This value MUST be accurate and the same value as used on the funnel definition in the Data Warehouse
  * Make sure to check with the person in charge of the DWH what this value should be
  * Alternatively, just leave it empty and use the URL for funnel association
* `data-acuity-redirect-url="XXX"` (Optional):
  * The URL where a person should be redirected to after filling the form
  * This will store this URL on the appointment in a custom field (by default `field:9230971`)
  * The acuity confirmation page has a JS tracking code that redirects the client to a WP redirect script with the appointment ID
  * The WP page uses the acuity API to fetch the appointment info using the appointment ID
  * It then redirects to the URL in the custom field or to the default URL

In addition to that you can add `data-attr-` attributes like `data-attr-style`. These attributes will be added without the `data-attr-` to the iFrame itself.

For example, if you add `data-attr-style="width: 100%"`, it will be added to the iFrame like:

```html
<iframe src="//app.acuityscheduling.com/..."  style="width: 100%"></iframe>
```

## How to test it

* Visit the page with the embedded acuity form.
* Right click somewhere on the appointment selection and select `Inspect`
* Search for `field:8644476` and look for a line like:

```html
<input type="hidden" name="field:8644476" value="{"ga_client_id":"","device_os":"","all_traffic_sources":"" [...]>
```

* Check that the data in `value` is what you expect it to be.
* Then make an appointment and check it in acuity scheduler's web UI
* Make sure it has the tracking data you expect
