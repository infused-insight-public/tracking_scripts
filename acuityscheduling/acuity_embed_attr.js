// IMPORTANT: 
// It must be put on the page AFTER the GAConnector or iTracker code

function getUrlParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.href);
    return results === null ? null : decodeURIComponent(results[1].replace(/\+/g, '    '));
};

function get_tracking_data() {

    var gaconnector_key_map = {
        GA_Client_ID: "ga_client_id",
        OS: "device_os",
        all_traffic_sources: "all_traffic_sources",
        browser: "device_browser",
        city: "location_city",
        country: "location_country",
        country_code: "location_country_code",
        device: "device_type",
        fc_campaign: "fc_campaign",
        fc_channel: "fc_channel",
        fc_content: "fc_content",
        fc_landing: "fc_landing",
        fc_medium: "fc_medium",
        fc_referrer: "fc_referrer_url",
        fc_source: "fc_source",
        fc_term: "fc_term",
        gclid: "gclid",
        ip_address: "ip_address",
        latitude: "latitude",
        lc_campaign: "utm_campaign",
        lc_channel: "utm_channel",
        lc_content: "utm_content",
        lc_landing: "landing_url",
        lc_medium: "utm_medium",
        lc_referrer: "utm_referrer",
        lc_source: "utm_source",
        lc_term: "utm_term",
        longitude: "longitude",
        page_visits: "page_visit_count",
        pages_visited_list: "pages_visited_list",
        region: "location_region",
        time_passed: "time_passed",
        time_zone: "time_zone",
        // Not actually a gac key. Added below to data.
        data_source: 'data_source',
        event_url: 'event_url',
    };

    tracking_data = {};
    if(window.gaconnector !== undefined) {

        tracking_data = window.gaconnector.getValues();
        tracking_data['data_source'] = 'gaconnector';
    } else if(window.iTracker360 !== undefined) {

        console.error('Tracking Attribution - ERROR: gaconnector not on page. Using iTracker data.');

        tracking_data['lc_medium'] = window.iTracker360.i.med;
        tracking_data['lc_source'] = window.iTracker360.i.sou;
        tracking_data['lc_campaign'] = window.iTracker360.i.cam;
        tracking_data['lc_term'] = window.iTracker360.i.ter;
        tracking_data['lc_content'] = window.iTracker360.i.con;
        tracking_data['lc_landing'] = window.iTracker360.i.firstlpurl;
        tracking_data['data_source'] = 'itracker360';
    } else {

        console.error('Acuity Attribution - ERROR: Neither gaconnector nor iTracker are on page. Will try to use query parameters.');
        tracking_data['lc_medium'] = (
            getUrlParameter('utm_medium') || 
            getUrlParameter('ga_medium') || 
            ''
        );
        tracking_data['lc_source'] = (
            getUrlParameter('utm_source') || 
            getUrlParameter('ga_source') || 
            ''
        );
        tracking_data['lc_campaign'] = (
            getUrlParameter('utm_campaign') || 
            getUrlParameter('ga_campaign') || 
            ''
        );
        tracking_data['lc_term'] = (
            getUrlParameter('utm_term') || 
            getUrlParameter('ga_term') || 
            ''
        );
        tracking_data['lc_content'] = (
            getUrlParameter('utm_content') || 
            getUrlParameter('ga_content') || 
            ''
        );
        tracking_data['data_source'] = 'browser_url';
    }
    tracking_data['event_url'] = window.location.href;

    json_data = {};
    for(const [gac_key, json_key] of Object.entries(gaconnector_key_map)) {
        json_data[json_key] = tracking_data[gac_key] || '';
    }

    return json_data;
}

function embed_acuity_with_tracking() {
    
    var default_acuity_attr_data_field_id = '8644476';
    var default_acuity_redirect_url_field_id = '9230971';

    var acuity_containers = document.getElementsByClassName("acuity-container");

    for (var i = 0; i < acuity_containers.length; i++) {
        acuity_container = acuity_containers[i];

        // Get container settings
        var acuity_owner = acuity_container.getAttribute('data-acuity-owner');
        var acuity_appointment_type = acuity_container.getAttribute('data-acuity-appointment-type');
        var funnel_step_id = acuity_container.getAttribute('data-funnel-step');

        // Attribution data field name
        var acuity_attr_data_field_id = acuity_container.getAttribute('data-acuity-attr_data_field_id') || default_acuity_attr_data_field_id;
        var acuity_attr_data_field_name = "field:" + acuity_attr_data_field_id;

        // Redirect URL
        var redirect_url = acuity_container.getAttribute('data-acuity-redirect-url');

        // Redirect URL field name
        var acuity_redirect_url_field_id = acuity_container.getAttribute('data-acuity-redirect_url_field_id') || default_acuity_redirect_url_field_id;
        var acuity_redirect_url_field_name = "field:" + acuity_redirect_url_field_id;

        if(!acuity_owner || !acuity_appointment_type) {

            console.error('Acuity Attribution - ERROR: Missing settings for acuity container: ' + acuity_container);
            continue;
        }

        if(!funnel_step_id) {

            console.warn('Acuity Attribution: Funnel step not set in acuity container:' + acuity_container);
        }

        var tracking_data = get_tracking_data();
        tracking_data['funnel_step_id'] = funnel_step_id;
        tracking_data_json = JSON.stringify(tracking_data);
        tracking_data_json_urlencoded = encodeURIComponent(tracking_data_json);

        // Generate acuity iframe URL
        params = {
            owner: acuity_owner,
            appointmentType: acuity_appointment_type,
        }
        params[acuity_attr_data_field_name] = tracking_data_json_urlencoded;

        if(redirect_url) {
            var redirect_url_urlencoded = encodeURIComponent(redirect_url);
            params[acuity_redirect_url_field_name] = redirect_url_urlencoded;
        } else {
            console.warn('Missing acuity redirect URL');
        }

        query_str = Object.keys(params).map(key => `${key}=${params[key]}`).join('&');

        var current_query_str = '';
        try {
            if(window.location.search.length > 0) {
                current_query_str = '&' + window.location.search.substring(1);
            }
        } catch(err) {

        } 
        query_str = query_str + current_query_str;

        iframe_url = '//app.acuityscheduling.com/schedule.php?' + query_str;

        // Create iFrame
        var ifrm = document.createElement('iframe');
        ifrm.setAttribute('src', iframe_url);

        // Add attributes to iFrame
        // Find container attributes that start with data-attr- 
        // and add them to the iFrame
        Array.from(acuity_container.attributes).forEach(attr => {

            if (attr.nodeName.substring(0, "data-attr-".length) == "data-attr-") {
                attr_name = attr.nodeName.substring("data-attr-".length);
                ifrm.setAttribute(attr_name, attr.nodeValue);
            }
        });

        // Add iFrame to container
        acuity_container.appendChild(ifrm);
    }
}

embed_acuity_with_tracking();
