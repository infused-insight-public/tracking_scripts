import json
from zapier_json_generator import process_zapier_data

test_data_html_escaped_json_str = {
    'signup_system': 'ThriveCart Checkout',

    'attribution_data': '{&quot;utm_medium&quot;:&quot;cpc&quot;,&quot;utm_source&quot;:&quot;YouTube&quot;,&quot;utm_campaign&quot;:&quot;11600494720&quot;,&quot;utm_term&quot;:&quot;-&quot;,&quot;utm_content&quot;:&quot;-&quot;,&quot;landing_url&quot;:&quot;https://challenge.marisapeer.com/abundance/?utm_source=YouTube&amp;utm_medium=cpc&amp;utm_campaign=11600494720&amp;utm_content=482668503273&amp;gclid=CjwKCAiA8Jf-BRB-EiwAWDtEGpHjyltholyXuJierZ_KOMUhIoj0n6Ak_MfrXulpMr-nY2LlizAVNxoC4M4QAvD_BwE&quot;,&quot;order_url&quot;:&quot;https://challenge.marisapeer.com/abundance/?utm_source=YouTube&amp;utm_medium=cpc&amp;utm_campaign=11600494720&amp;utm_content=482668503273&amp;gclid=CjwKCAiA8Jf-BRB-EiwAWDtEGpHjyltholyXuJierZ_KOMUhIoj0n6Ak_MfrXulpMr-nY2LlizAVNxoC4M4QAvD_BwE&quot;}',

    'email': 'kat82har@gmail.com',

    'purchase_name': 'Abundance Challenge',

    'customer_id': 33360062,

    'ip_address': '172.58.4.122',

    'payment_processor': 'stripe',

    'gross_total': 99.00,

    'purchase_amount': 99.00,

    'invoice_id': "001822540",

    'product_id': 493,
}

test_data_python_obj = {
    'signup_system': 'ThriveCart Checkout',

    'attribution_data': {
        'utm_medium': 'cpc',
        'utm_source': 'YouTube',
        'utm_campaign': '11600494720',
        'utm_term': '-',
        'utm_content': '-',
        'landing_url': 'https://challenge.marisapeer.com/abundance/?utm_source=YouTube&utm_medium=cpc&utm_campaign=11600494720&utm_content=482668503273&gclid=CjwKCAiA8Jf-BRB-EiwAWDtEGpHjyltholyXuJierZ_KOMUhIoj0n6Ak_MfrXulpMr-nY2LlizAVNxoC4M4QAvD_BwE',
        'order_url': 'https://challenge.marisapeer.com/abundance/?utm_source=YouTube&utm_medium=cpc&utm_campaign=11600494720&utm_content=482668503273&gclid=CjwKCAiA8Jf-BRB-EiwAWDtEGpHjyltholyXuJierZ_KOMUhIoj0n6Ak_MfrXulpMr-nY2LlizAVNxoC4M4QAvD_BwE'
    },

    'email': 'kat82har@gmail.com',

    'purchase_name': 'Abundance Challenge',

    'customer_id': 33360062,

    'ip_address': '172.58.4.122',

    'payment_processor': 'stripe',

    'gross_total': 99.00,

    'purchase_amount': 99.00,

    'invoice_id': "001822540",

    'product_id': 493,
}

# output should have no JSON errors
# and the data value should be a json string (but not a JSON object tree)
result_html_escaped_json_str = json.dumps(
    process_zapier_data(test_data_html_escaped_json_str)
)
print(f'Unescaped json str result: {result_html_escaped_json_str}\n\n')

result_python_obj = json.dumps(
    process_zapier_data(test_data_python_obj)
)
print(f'Python obj result: {result_python_obj}\n\n')
