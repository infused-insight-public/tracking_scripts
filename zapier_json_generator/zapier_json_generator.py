import json
import html


def unescape_str(value):
    # Sometimes nested data comes in as a string with HTML escaped characters
    # like `'{&quot;utm_medium&quot;:&quot;cpc&quot;}`
    # So here we attempt to unescape it
    try:
        value_unescaped = html.unescape(value)
    except:
        value_unescaped = value

    return value_unescaped


def json_decode_str(value):
    # Decode JSON strings into python objects
    # This will ensure they end up as valid json
    # in the final output instead of a string of
    # json
    try:
        value_decoded = json.loads(value)
    except:
        value_decoded = value

    return value_decoded


def decode_value(value):
    value_unescaped = unescape_str(value)
    value_decoded = json_decode_str(value_unescaped)

    return value_decoded


def process_zapier_data(input_data):
    input_decoded = {}

    for key, value in input_data.items():
        input_decoded[key] = decode_value(value)

    # Convert python object into nicely formatted json string
    output_json_str = json.dumps(input_decoded, indent=4, sort_keys=True)

    # Provide output to next task
    output = {
        'data': output_json_str,
    }

    return output



output = process_zapier_data(input_data)
